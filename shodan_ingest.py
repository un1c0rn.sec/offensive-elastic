#!/usr/bin/env python3

import json
import requests
import argparse


def main():
    parser = argparse.ArgumentParser(description="Ingest shodan json export files into ElasticSearch indexes")
    parser.add_argument('in_file', help='Input file. Needs to be default shodan export (multiple dictionaries in '
                                        'single file).')
    parser.add_argument('elastic_index', help='Target index on the elastic target.')
    parser.add_argument('--host', help='Set specific elastic host (default: localhost)', default='127.0.0.1')
    parser.add_argument('--port', help='Set specific port for elastic host (default: 9200)', default='9200')
    parser.add_argument('--username', help='Set Username for elastic host (default: None)')
    parser.add_argument('--password', help='Set Password for elastic host (default: None)')
    parser.add_argument('--useSSL', help='Enable SSL for communication with elastic host', action="store_true")
    parser.add_argument('--ignoreCert', help='Ignore SSL certificate verification', action="store_true")
    args = parser.parse_args()

    # Initialize base variables
    if args.useSSL:
        url = f"https://{args.host}:{args.port}/{args.elastic_index}/_doc"
    else:
        url = f"http://{args.host}:{args.port}/{args.elastic_index}/_doc"
    headers = {"Content-type": "application/json"}

    # Shodan export files have a single dictionary per line in file.
    # Iterate through each line in file, post dictionary "document" to the index
    # SSL serial can be huge and is of near zero value. If it exists, remove it
    with open(args.in_file) as input_file:
        for each_object in input_file:
            individual_object = json.loads(each_object)
            if individual_object.get('ssl'):
                del individual_object['ssl']['cert']['serial']
            if args.useSSL:
                if args.username is not None:
                    if args.ignoreCert:
                        r = requests.post(url, headers=headers, json=individual_object, auth=(args.username, args.password), verify=False)
                    else:
                        r = requests.post(url, headers=headers, json=individual_object, auth=(args.username, args.password))
                else:
                    if args.ignoreCert:
                        r = requests.post(url, headers=headers, json=individual_object, verify=False)
                    else:
                        r = requests.post(url, headers=headers, json=individual_object)
            else:
                r = requests.post(url, headers=headers, json=individual_object)
            print(r.json())


if __name__ == '__main__':
    main()
