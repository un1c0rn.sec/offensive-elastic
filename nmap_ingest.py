#!/usr/bin/env python3

import json
import requests
import argparse
import xmltodict
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

NMAP_ELASTIC_MAPPING = {
    "mappings": {
        "properties": {
            "join_field": {
              "type": "join",
              "relations": {
                  "host": ["open_port", "hostname", "scan"]
              }
            },
            "port_num": {
                "type": "keyword",
                "index": True
            },
            "ip": {
                "type": "ip"
            },
            "date": {
                "type": "date",
                "format": "epoch_second"
            }
        }
    }
}


def xml2json(xml):
    xmlfile = open(xml)
    xml_content = xmlfile.read()
    xmlfile.close()
    xmljson = json.dumps(xmltodict.parse(xml_content), indent=4, sort_keys=True)
    jsondata = json.loads(xmljson)
    return jsondata


def build_es_host_json(ip):
    # Creates host object from nmap scan for the es index
    # A host object is just a collector for other data. Using ip as the identifier.
    return {'join_field': 'host', 'ip': ip}


def build_es_hostname_json(ip, hostname, scan_date):
    # Creates hostname object from nmap scan
    # Returns hostname json for importing into es
    return {"join_field": {"name": "hostname", "parent": ip}, 'ip': ip, 'hostname': hostname, 'scan_type': 'nmap',
            'date': scan_date}


def build_es_port_json(ip, port_dict, scan_date):
    # Returns a single open port object for importing into es
    port_num = port_dict['@portid']
    del port_dict['@portid']
    try:
        if isinstance(port_dict['script'], list):
            index = 0
            for item in port_dict['script']:
                try:
                    del port_dict['script'][index]['elem']
                except KeyError:
                    pass
                try:
                    del port_dict['script'][index]['table']
                except KeyError:
                    pass
                index = index + 1
        else:
            try:
                del port_dict['script']['elem']
            except KeyError:
                pass
    except KeyError:
        pass
    port_details = port_dict
    return {'join_field': {"name": "open_port", "parent": ip}, 'ip': ip, 'port_num': port_num,
            'port_details': port_details, 'scan_type': 'nmap', 'date': scan_date}


def build_es_scan_json(nmap_single_host_json):
    ip = nmap_single_host_json['address']['@addr']
    date = nmap_single_host_json['@starttime']
    return {"join_field": {"name": "scan", "parent": ip}, 'ip': ip, 'scan_type': 'nmap', 'date': date}


def initialize_index(mapping, url, use_ssl=True, ignore_cert=False, username=None, password=None):
    headers = {"Content-type": "application/json"}
    if use_ssl:
        if username is not None:
            r = requests.put(f"{url}", headers=headers, data=json.dumps(mapping), auth=(username, password),
                             verify=ignore_cert)
        else:
            r = requests.put(f"{url}", headers=headers, data=json.dumps(mapping), verify=ignore_cert)
    else:
        r = requests.put(f"{url}", headers=headers, data=json.dumps(mapping))
    return r.json()


def send_to_es(object_to_send, url, use_ssl=True, username=None, password=None, ignore_cert=False, doc_id=""):
    headers = {"Content-type": "application/json"}
    if use_ssl:
        if username is not None:
            r = requests.post(f"{url}/_doc{doc_id}", headers=headers, json=object_to_send, auth=(username, password),
                              verify=ignore_cert)
        else:
            r = requests.post(f"{url}/_doc{doc_id}", headers=headers, json=object_to_send, verify=False)
    else:
        r = requests.post(f"{url}/_doc{doc_id}", headers=headers, json=object_to_send)
    return r.json()


def get_host_list(nmap_json):
    if isinstance(nmap_json['nmaprun']['host'], dict):
        host_list = [nmap_json['nmaprun']['host']]
    else:
        host_list = nmap_json['nmaprun']['host']

    return host_list


def main():
    parser = argparse.ArgumentParser(description="Ingest nmap xml export files into ElasticSearch indexes")
    parser.add_argument('in_file', help='Input file. Needs to be default nmap XML files')
    parser.add_argument('elastic_index', help='Target index on the elastic target.')
    parser.add_argument('--host', help='Set specific elastic host (default: localhost)', default='127.0.0.1')
    parser.add_argument('--port', help='Set specific port for elastic host (default: 9200)', default='9200')
    parser.add_argument('--username', help='Set Username for elastic host (default: None)')
    parser.add_argument('--password', help='Set Password for elastic host (default: None)')
    parser.add_argument('--useSSL', help='Enable SSL for communication with elastic host', action="store_true")
    parser.add_argument('--ignoreCert', help='Ignore SSL certificate verification', action="store_true")
    parser.add_argument('--new_index', help='Index does not exist yet (default: False)', action="store_true",
                        default=False)
    args = parser.parse_args()

    # Initialize base variables
    if args.useSSL:
        url = f"https://{args.host}:{args.port}/{args.elastic_index}"
    else:
        url = f"http://{args.host}:{args.port}/{args.elastic_index}"
    ignore_cert = not args.ignoreCert

    # Initialize new nmap index and mapping
    if args.new_index:
        print(f"Creating new index: {args.elastic_index}")
        initialization_response = initialize_index(NMAP_ELASTIC_MAPPING, url, use_ssl=args.useSSL,
                                                   ignore_cert=ignore_cert, username=args.username,
                                                   password=args.password)
        print(initialization_response)

    # Convert XML Nmap to Json, Parse out hosts from the nmap run
    nmap_json = xml2json(args.in_file)
    nmap_host_list = get_host_list(nmap_json)

    for host in nmap_host_list:
        # For each host in a scan, parse out objects to be indexed and index them

        # A host is a parent object, needs a unique doc_id, using the IP address
        ip = host['address']['@addr']

        # Ingest host objects
        host_object = build_es_host_json(ip)
        response = send_to_es(host_object, url, use_ssl=args.useSSL, username=args.username, password=args.password,
                              ignore_cert=ignore_cert, doc_id=f"/{ip}")
        print(response)

        # Ingest scan objects
        scan_object = build_es_scan_json(host)
        response = send_to_es(scan_object, url, use_ssl=args.useSSL, username=args.username, password=args.password,
                              ignore_cert=ignore_cert, doc_id=f"?routing={ip}")
        print(response)

        # Ingest hostname objects
        if host['hostnames'] is not None:
            if isinstance(host['hostnames'], dict):
                hostname_list = [host['hostnames']]
            else:
                hostname_list = host['hostnames']

            for hostname in hostname_list:
                # Create Hostname Object
                hostname_object = build_es_hostname_json(ip, hostname['hostname']['@name'], host['@starttime'])
                # Send Hostname Objects Individually
                response = send_to_es(hostname_object, url, use_ssl=args.useSSL, username=args.username,
                                      password=args.password, ignore_cert=ignore_cert, doc_id=f"?routing={ip}")
                print(response)

        # Ingest port objects
        if host['ports']['port'] is not None:
            if isinstance(host['ports']['port'], dict):
                ports_list = [host['ports']['port']]
            else:
                ports_list = host['ports']['port']

            for port in ports_list:
                port_object = build_es_port_json(ip, port, host['@starttime'])
                response = send_to_es(port_object, url, use_ssl=args.useSSL, username=args.username,
                                      password=args.password, ignore_cert=ignore_cert, doc_id=f"?routing={ip}")
                print(response)


if __name__ == '__main__':
    main()
