# Offensive Elastic

Scripts and utilities to stand up and load offensive tool output into elasticstack.

### Todos  

##### MVP  
  
* Make shodan_ingest.py more resilient (check for formatting, ingest different types of formatting, fail gracefully)  
* Create bootstrap tool/script/function to create new index and correct mapping for use case
* Create nmap ingester (convert xml to json, then send it)
* Create WART ingester
  
##### Stretch
  
* Utilize shodan api to seamlessly run a query and insert it into an index (and bootstrap if index doesn't exist)
  
### Utilities  
  
##### shodan_ingest.py:
```
usage: shodan_ingest.py [-h] [--host HOST] [--port PORT] in_file elastic_index

Ingest shodan json export files into ElasticSearch indexes

positional arguments:
  in_file        Input file. Needs to be default shodan export (multiple
                 dictionaries in single file).
  elastic_index  Target index on the elastic target.

optional arguments:
  -h, --help     show this help message and exit
  --host HOST    Set specific elastic host (default: localhost)
  --port PORT    Set specific port for elastic host (default: 9200)
  ```
